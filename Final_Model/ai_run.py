import tensorflow as tf
import joblib
import os
from tensorflow import keras
import joblib
from tensorflow.keras.applications.resnet50 import ResNet50
from keras.preprocessing.image import load_img, img_to_array
import requests
import json
from keras.preprocessing import image
from os.path import join
from keras.preprocessing.image import ImageDataGenerator 
import numpy as np
from tensorflow.keras import Sequential
from tensorflow.keras.applications import xception
from tensorflow.keras import layers, models, optimizers 
from tensorflow.keras.applications.vgg16 import preprocess_input
import shutil
import sys
import logging
import psycopg2 # postgreSQL 연동 모듈 import
import pandas as pd
import datetime
import traceback
import sklearn
def getTracebackStr():
    
	lines = traceback.format_exc().strip().split('\n')
	rl = [lines[-1]]
	lines = lines[1:-1]
	lines.reverse()
	nstr = ''
	for i in range(len(lines)):
		line = lines[i].strip()
		if line.startswith('File "'):
			eles = lines[i].strip().split('"')
			basename = os.path.basename(eles[1])
			lastdir = os.path.basename(os.path.dirname(eles[1]))
			eles[1] = '%s/%s' % (lastdir,basename)
			rl.append('^\t%s %s' % (nstr,'"'.join(eles)))
			nstr = ''
		else:
			nstr += line
	log_message('\n'.join(rl)) 
    
    
def get_cursor():
    # Connect to an existing database
    host = '211.218.53.149'
    dbname = 'Naver'
    user = 'postgres'
    pwd = 'dpafhqh1!'
    conn = psycopg2.connect('host={0} dbname={1} user={2} password={3}'.format(host, dbname, user, pwd))
    cur = conn.cursor()
    return cur, conn

def insert_report(report_id, age, species):
    try:
        age = str(age)
        species = str(species)
        cur, conn = get_cursor()
        cur.executemany("INSERT INTO detective.report(report_id, age, species)\
                    VALUES (%s, %s, %s)", [[report_id, age, species]])
        conn.commit()
    except:
        getTracebackStr()
        
        log_message(str(sys.exc_info()))  

def operation_logging():
    directory = '/home/mrobo/Desktop/NaverHackathon/Final_Model/log/'
    if not os.path.exists(directory):       
        os.makedirs(directory) 
    logger = logging.getLogger('AI') 
    logger.setLevel(logging.DEBUG)
    if (logger.hasHandlers()):
        logger.handlers.clear()
    file_handler = logging.FileHandler(filename=(directory + "ai.log") , encoding='utf-8') 
    streamHandler = logging.StreamHandler()
    logger.addHandler(file_handler)
    logger.addHandler(streamHandler)

    return logger

def log_message(message):
    message = str(message)
    operation_logger = operation_logging()
    FMT = '%m%d-%H:%M:%S'
    logger_now = datetime.datetime.now().strftime(FMT)
    operation_logger.info("from " + message + " " + logger_now)

breed_answer = {0: 'beagle',
                1: 'border_collie',
                2: 'french_bulldog',
                3: 'german_shepherd',
                4: 'golden_retriever',
                5: 'great_pyrenees',
                6: 'labrador_retriever',
                7: 'malamute',
                8: 'maltese_dog',
                9: 'miniature_poodle',
                10: 'pug',
                11: 'rottweiler',
                12: 'toy_poodle',
                13: 'yorkshire_terrier'}


def api_request(image_name, data_dir):
    client_id = '4fcdevzg54'
    client_secret = 'R8QC2jttW4g4h2lbcdMuSBlYwBGfqZmSDsOyNIkl'

    url = 'https://naveropenapi.apigw.ntruss.com/vision-obj/v1/detect'

    files = {
        'image': open(data_dir + image_name, 'rb')
    }

    headers = {
        'X-NCP-APIGW-API-KEY-ID': client_id,
        'X-NCP-APIGW-API-KEY': client_secret
    }

    response = requests.post(url, files=files, headers=headers)
    rescode = response.status_code

    if rescode == 200:
        return response.text
    else:
        print("Error Code", rescode)
        
def extract_box(info_text):
    info_dict = json.loads(info_text)
    try:
        i = info_dict['predictions'][0]['detection_names'].index('dog')
        return  info_dict['predictions'][0]['detection_boxes'][i]
    except:
        return [0, 0, 1, 1]
    
def crop(img, r):
    w, h = img.size
    crop_area = (w*r[1], h*r[0], w*r[3], h*r[2])
    
#     print(crop_area)
    img = img.crop(crop_area)
    return img

def read_img_crop(img_id, data_dir, size):

    # api => image dog box
    img_info = api_request(img_id + '.jpg', data_dir)
    box = extract_box(img_info)

    img = image.load_img(join(data_dir, '%s.jpg' % img_id))
    # img = image.load_img(join(data_dir, train_or_test, '%s.jpg' % img_id), target_size=size)

    img = crop(img, box)
    img = img.resize(size)
#     img = image.img_to_array(img)
    return img

def extract_feature(path, resnet_model):
    imgs =[]
    for i in range(len(os.listdir(path))):
        if '.ipy' not in os.listdir(path)[i] :
            img = load_img(path+os.listdir(path)[i], target_size =(224, 224))
            imgs.append(img)
    img_array = np.array([img_to_array(img) for img in imgs])
    output = preprocess_input(img_array)
    output = resnet_model.predict(output)
    return output

def create_model():
    model = Sequential()
    model.add(tf.keras.applications.xception.Xception(input_shape=(299, 299, 3), include_top=False, weights='imagenet'))
#     model.add(tensorflow.keras.applications.ResNet50(input_shape=(INPUT_SIZE, INPUT_SIZE, 3), include_top=False, weights='imagenet'))
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(256, activation='sigmoid'))
    model.add(layers.Dropout(0.25))
    model.add(layers.Dense(14, activation='softmax'))
    optimizer = optimizers.Adam()
    model.layers[0].trainable = False

    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
    return model

def breed_age_predict():
    log_message('start')
    # Crop Image Generation and Save
    orig_data_dir = '/home/mrobo/Desktop/NaverHackathon/Final_Model/original_test_data/'
    crop_data_dir = '/home/mrobo/Desktop/NaverHackathon/Final_Model/crop_test_data/'
    file_name = os.listdir(orig_data_dir)[0].split('.')[0] # file_name 확장자 없이
    cropped_img = read_img_crop(file_name, orig_data_dir, (299, 299))
#     cropped_img.save(crop_data_dir + file_name + '.jpg')
    cropped_img.save(crop_data_dir + 'tmp/' +file_name + '.jpg')
#     read_img_crop(file_name, orig_data_dir, (299, 299)).save(crop_data_dir + file_name + '.jpg')
    log_message('123')
    # Breed Classification

    # Breed Model Load
#     saved_model_path = './breed_model_selected/'
#     breed_model = tf.keras.models.load_model(saved_model_path)
    
    #Inference breed 
    test_datagen = ImageDataGenerator(rescale=1./255)

    log_message('0')
    test_generator = test_datagen.flow_from_directory(
                    crop_data_dir,
                    target_size=(299, 299),
                    batch_size=1,
                    class_mode=None,
                    shuffle=False)
    
    breed_model = create_model()
    log_message('1')
    breed_model.load_weights('/home/mrobo/Desktop/NaverHackathon/Final_Model/breed_classification_weights_final.h5')

    pred = breed_model.predict_generator(generator=test_generator)
    pred_class = np.argmax(pred, axis=1)
    log_message('2')
    breed = breed_answer[pred_class[0]] # breed 결과물 string으로 return
    # 전처리
    resnet_model = ResNet50(input_shape=(224, 224, 3), weights='imagenet', include_top=False, pooling='avg')
    x_test = extract_feature(crop_data_dir+'tmp/', resnet_model)
    log_message('3')
    # Breed에 맞는 Age Classification
    if breed == 'beagle':
        ss = joblib.load('/home/mrobo/Desktop/NaverHackathon/Final_Model/beagle_ss')
        pca = joblib.load('/home/mrobo/Desktop/NaverHackathon/Final_Model/beagle_pca')
        
        x_test = ss.transform(x_test)
        x_test = pca.transform(x_test)
        
        age_model = joblib.load('/home/mrobo/Desktop/NaverHackathon/Final_Model/beagle_age_classifier_lgb.pkl')
        print(age_model.predict(x_test))

        age = int(round(age_model.predict(x_test)[0]))
    elif breed == 'border_collie':
        
        ss = joblib.load('/home/mrobo/Desktop/NaverHackathon/Final_Model/collie_ss')
        
        x_test = ss.transform(x_test)
        
        age_model = joblib.load('/home/mrobo/Desktop/NaverHackathon/Final_Model/collie_age_classifier_lgb.pkl')
        age = int(round(age_model.predict(x_test)[0]))
        
    else: # beagle, border_collie 아니면 전부 4 리턴
        age = 4
    
    # Age Model Load (string concat을 통해 맞는 모델 호출)
    
    # 폴더 초기화
#     os.remove(orig_data_dir + file_name + '.jpg')
#     os.remove(crop_data_dir + 'tmp/' + file_name + '.jpg')
    
    return breed, age

if __name__ == "__main__":
    try:
        arguments = sys.argv
        report_id = arguments[1]
        log_message(str(report_id))
        breed, age = breed_age_predict()
        insert_report(report_id, age, breed)
        
    except:
        getTracebackStr()
        log_message(str(sys.exc_info()))