import psycopg2 # postgreSQL 연동 모듈 import
import pandas as pd

def get_cursor():
    # Connect to an existing database
    host = '211.218.53.149'
    dbname = 'Naver'
    user = 'postgres'
    pwd = 'dpafhqh1!'
    conn = psycopg2.connect('host={0} dbname={1} user={2} password={3}'.format(host, dbname, user, pwd))
    cur = conn.cursor()
    return cur, conn

def insert_upload(report_id, data_path):
    try:
        cur, conn = get_cursor()
        cur.executemany("INSERT INTO detective.upload(report_id, file_path) \
                    VALUES (%s, %s)", [[report_id, data_path]])
        conn.commit()
    except:
        print(str(sys.exc_info()))
def insert_report(report_id, age, species):
    try:
        cur, conn = get_cursor()
        cur.executemany("INSERT INTO detective.report(report_id, age, species)\
                    VALUES (%s)", [[report_id, age, species]])
        conn.commit()
    except:
        print(str(sys.exc_info()))  
        
def check_report(report_id):
    cur, conn = get_cursor()
    query = '''
    select * from detective.report where report_id='{report_id}'
    '''.format(report_id = report_id)


    # column_name = 
    cur.execute(query)
    colnames = [desc[0] for desc in cur.description]
    a = cur.fetchall()
    data_df = pd.DataFrame(a, columns = colnames)
    return data_df

def check_upload(report_id):
    cur, conn = get_cursor()
    query = '''
    select * from detective.upload where report_id='{report_id}'
    '''.format(report_id = report_id)


    # column_name = 
    cur.execute(query)
    colnames = [desc[0] for desc in cur.description]
    a = cur.fetchall()
    data_df = pd.DataFrame(a, columns = colnames)
    return data_df