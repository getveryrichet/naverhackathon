# -- coding: utf-8 --


from flask_sslify import SSLify
from flask import Flask, jsonify, request, render_template, flash, redirect
from flask_restful import Resource, Api, reqparse
# from resources.test import Test
from flask_cors import CORS
from shutil import copyfile
from flask_restful.utils import cors
import requests
import shutil
import pandas as pd
import json
import numpy as np
import re
import datetime
from time import sleep
import inspect
import sys
import logging
import time
import os
import base64
import subprocess
params = {'host':'localhost', 'dbname':'KISA', 'user':'postgres', 'pwd':'dpafhqh1!'}
host = params['host']
dbname = params['dbname']
user = params['user']
pwd = params['pwd']
import psycopg2
import ssl
from OpenSSL import SSL
from flask_talisman import Talisman
import werkzeug
from werkzeug.utils import secure_filename
import uuid 

#self_module
import detective_db
# import Final_Model.ai_run as ai
from dogdata.dog import beagle, border_collie



MODEL_UPLOAD_FOLDER = os.getcwd() + '/Final_Model/original_test_data/'
UPLOAD_FOLDER = '/home/mrobo/Desktop/NaverHackathon/image'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DEBUG'] = True
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['CORS_ENABLED'] = True
# CORS(app)
CORS(app, allow_headers=["Content-Type", "Authorization", "Access-Control-Allow-Credentials", "Access-Control-Allow-Origin"], supports_credentials=True)

sslify = SSLify(app)
Talisman(app)


api = Api(app)


# if app.config['CORS_ENABLED'] is True:
#     CORS(app, origins="*", allow_headers=[
#         "Content-Type", "Authorization", "Access-Control-Allow-Credentials"],
#         supports_credentials=True)
    
    
file_handler = logging.FileHandler('server.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def move_pic(data_path):
    shutil.copy(data_path, '/home/mrobo/Desktop/NaverHackathon/Final_Model/original_test_data/')
    
def reset_directory():
    
    shutil.rmtree("/home/mrobo/Desktop/NaverHackathon/Final_Model/crop_test_data/", ignore_errors=True)
    shutil.rmtree("/home/mrobo/Desktop/NaverHackathon/Final_Model/original_test_data", ignore_errors=True)
    os.mkdir("/home/mrobo/Desktop/NaverHackathon/Final_Model/original_test_data")
    os.mkdir("/home/mrobo/Desktop/NaverHackathon/Final_Model/crop_test_data")
    os.mkdir("/home/mrobo/Desktop/NaverHackathon/Final_Model/crop_test_data/tmp")
    
    
class HOME(Resource):
    def get(self):
        try:
            json_doc = {'Greeting' : 'Hello'}
            return jsonify(json_doc)

        except Exception as e:
            return {'error':str(e)}
        
   




        return jsonify({'message' : 'success'})

 
class Test2(Resource):
    
    def post(self):
        '''form으로 가져와서 image랑 Id 파싱'''

        try:
            app.logger.info('SendImageSection ---------------')
            app.logger.info('Post requested ' + str(datetime.datetime.now()))
            content = request.files
            app.logger.info("image downloaded " + str(datetime.datetime.now()))
            app.logger.info(str(content) + str(datetime.datetime.now()))   
            
            report_id = str(uuid.uuid1())
            
            #파일 첨부 안됐을 경우
            if 'image' not in content:
                app.logger.info('No image part')
                flash('No image part')
                return redirect(request.url)
        
            # if user does not select file, browser also
            # submit an empty part without filename
            f = content['image']
            
            
            if f.filename == '':
                app.logger.info('No selected file')
                flash('No selected file')
                return redirect(request.url)
            
            
            if f and allowed_file(f.filename):
                filename = secure_filename(f.filename)
                if not '.' in filename :
                    extensions = ''
                    if 'png' in filename:
                        extensions = '.png'
                    elif 'jpg' in filename:
                        extensions = '.jpg'
                    data_path = os.path.join(app.config['UPLOAD_FOLDER'], report_id[-4:]  + extensions)
                    app.logger.info("here1")
                    data_path2 = MODEL_UPLOAD_FOLDER + report_id[-4:] + filename + extensions
                    app.logger.info("here2")
                    
                else: 
                    data_path = os.path.join(app.config['UPLOAD_FOLDER'], report_id[-4:] + filename)
                    app.logger.info("here1")
                    data_path2 = MODEL_UPLOAD_FOLDER + report_id[-4:] + filename
                    app.logger.info("here2")
                f.save(data_path)
                app.logger.info(data_path)
                copyfile(data_path, data_path2)
                app.logger.info("copied")
                app.logger.info(data_path2)
                
                
                
#                 f.save(data_path2)
                

            
            
#             data_path = os.path.join(app.config['UPLOAD_FOLDER'], report_id[-4:] + filename )
            cur, conn = detective_db.get_cursor()
            detective_db.insert_upload(report_id, data_path)
            
            app.logger.info("report_id : " + report_id)
            app.logger.info("data_path : " + data_path)
            app.logger.info(f)
            
            reset_directory()
            move_pic(data_path)
            
            subprocess.Popen([('python /home/mrobo/Desktop/NaverHackathon/Final_Model/ai_run.py ' + str(report_id))], shell = True)
            
#             form에 id든 뭗느 내용 추가해서 저장하고 싶으면 아래 주석 풀 것.
#           content2 = request.form['id']
#             app.logger.info("data found" + str(datetime.datetime.now()))
#             app.logger.info(str(idx) + str(datetime.datetime.now()))
#             i = content2['id']
#             app.logger.info(i)
        
        
        except:
            app.logger.info("error2")
            app.logger.info("error2")
            app.logger.info("error2")
            
            app.logger.info(str(sys.exc_info()))
#         print(content)

        
#         if
        app.logger.info('SendImageSection ---------------')
        return jsonify({'message' : 'success', 'report_id' : report_id})
    
#         if


class Check(Resource):
    
    def post(self):   
        try:
            app.logger.info('CheckSection ---------------')
            report_id = request.form['report_id']
#             app.logger.info("check")
#             app.logger.info('form : '  + str(request.form['report_id']))
    #         app.logger.info(request.data)
#             app.logger.info("check")
    #     def get(self):   

    #         report_id = request.args.get('report_id')
            data = detective_db.check_report(report_id)

#             app.logger.info("report_id from check: " + str(report_id) + str(datetime.datetime.now()))

    #         app.logger.info(report_id)    
            
            if len(data) == 0 :
                message = False
                app.logger.info(str(report_id) + " not found yet" )
            if len(data) != 0 :
                message = True


#             app.logger.info('CheckSection ---------------')
            return jsonify({'is_data_set' : message})
        
        except:
            app.logger.info("error1")
            app.logger.info(str(sys.exc_info()))
            return jsonify({'is_data_set' : "error"})
            
#         print(content)    

        
# #         return jsonify({'image': file})
class GetReport2(Resource):
    def get(self):
        try:
            #repoort_id 읽고
            app.logger.info('GetReportSection ---------------')
            report_id = request.args.get('report_id')
    #         report_id = request.form['report_id']
            app.logger.info("report_id from getreport : " + str(report_id))
            #file_path 가져오기위해서 DB통해서 읽음
            data = detective_db.check_upload(report_id)
            dog = ''
            species = 'beagle'
            
            #견종에 따라서 dict 다시 만들기
            if species == 'beagle':
                dog = beagle
                dog['species'] = '비글'
            elif species == 'border_collie':
                dog = border_collie
                dog['species'] = '보더콜리'
               
            
            #이미지를 json으로 보내야해서 byte형태로 저장
            image_extension = ''
            if len(data) != 0 :
                image_path = data.file_path.iloc[0]
                if 'png' in image_path :
                    image_extension = 'png'
                elif 'jpg' in image_path : 
                    image_extension = 'jpg'
                elif 'jpeg' in image_path : 
                    image_extension = 'jpeg'         
                    
                app.logger.info(image_path)
                with open(image_path, "rb") as image_file:
                    encoded_string = base64.b64encode(image_file.read())
                    image_str = encoded_string.decode('utf-8')

#                 app.logger.info('encoded_string------')
#                 app.logger.info(encoded_string)
#                 app.logger.info('encoded_string------')


            else: 
                image_str = ''
            
            dog['age'] = str(0)
            dog['image'] = image_str
            dog['image_extension'] = image_extension

            app.logger.info("report_check" + str(datetime.datetime.now()))
            app.logger.info("report_id : " + str(report_id) + str(datetime.datetime.now()))
            app.logger.info(image_extension)
            app.logger.info(report_id)   

            #type2

            app.logger.info('GetReportSection ---------------')
            return jsonify(dog)
#             return jsonify(json.dumps(dog, ensure_ascii=False))
        except:
            app.logger.info(str(sys.exc_info()))

api.add_resource(HOME, '/')
# api.add_resource(Image, '/image')
api.add_resource(Test2, '/test')
api.add_resource(Check, '/check')   
    
api.add_resource(GetReport2, '/getreport')     



if __name__ == '__main__':
    
    cert = 'server.crt'
    pkey = 'server.key'
    context = (cert, pkey)
#     context = SSL.Context(SSL.TLSv1_2_METHOD)
#     context.use_privatekey_file('future.key')
#     context.use_certificate_file('future.crt')
#     context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
#     context.verify_mode = ssl.CERT_REQUIRED
#     context.load_verify_locations('ca-crt.pem')
#     context.load_cert_chain('server.crt', 'server.key')
    
 
    
    app.run(debug=True, host='0.0.0.0', port=5000,  ssl_context=context, threaded=True)