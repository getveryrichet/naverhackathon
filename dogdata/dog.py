import pandas as pd



border_diseases = [{
'name': '진행성 망막위축증',
'explanation': '망막이 위축되는 질병으로 망막 전체에서 위축이 진행되는 경우 망막의 중심부위에서 위축이 진행되는 경우로 나눌 수 있다.  망막위축은 실명의 원인이 된다.'
},{
'name': '뇌전증',
'explanation': '뇌전증이란 단일한 뇌전증 발작을 유발할 수 있는 원인 인자, 즉 전해질 불균형, 산-염기 이상, 요독증, 알코올 금단현상, 심한 수면박탈상태 등 발작을 초래할 수 있는 신체적 이상이 없음에도 불구하고, 뇌전증 발작이 반복적으로(24시간 이상의 간격을 두고 2회 이상) 발생하여 만성화된 질환군을 의미한다. 또는, 뇌전증 발작이 1회만 발생하였다고 하더라도 뇌 영상검사(뇌 MRI 등)에서 뇌전증을 일으킬 수 있는 병리적 변화가 존재하면 뇌전증으로 분류한다.'
},{
'name': '콜리 안구 기형',
'explanation': '선천성, 상염체열성으로 맥락막의 형성부전이나 일시적으로 원발성 망막 혈관이 과도하게 뒤틀린 기형'
},{
'name': '고관절 이형성증',
'explanation': '고관절 이형성증(hip dysplasia)은 고관절의 선천성, 발달성 형성 이상증이다.'
}]


border_foods= [{
'name': '닭곰탕',
'image_src': 'http://blogfiles.naver.net/20110318_97/303mani_1300401830762tjBPv_JPEG/DSCF0713.JPG',
'explanation': '관절에 좋은 음식'
},{
'name': '북엇국',
'image_src': 'https://blog.naver.com/wltnd87/221022202588',
'explanation': '관절에 좋은 음식'
},{
'name': '홍화씨가루',
'image_src': 'http://blogfiles.naver.net/MjAxNjEwMjhfMjM0/MDAxNDc3NjM0MjgxOTk2.xUd9secV3qEIxfX6m0okR4W_UEw83hyN9TetypXVswQg.RZ90woLTys_muYv7DixZivq3bXqwE1sLeU7g7c_5RnMg.JPEG.joytoseon/3.jpg',
'explanation': '관절에 좋은 음식'
},{
'name': '단호박',
'image_src': 'http://blogfiles.naver.net/20160315_151/cwr9441_1458048688963eOWuc_JPEG/NaverBlog_20160315_223128_04.jpg',
'explanation': '눈, 관절에 좋은 음식'
},{
'name': '시금치',
'image_src': 'http://blogfiles.naver.net/20130205_265/smy2166_1360023315539Fwi1N_JPEG/16.JPG',
'explanation': '눈에 좋은 음식'
},{
'name': '당근',
'image_src': 'http://blogfiles.naver.net/20110831_204/qkrwlsdyd28_1314768481649KvnVp_JPEG/2011-08-31_14%3B26%3B24.jpg',
'explanation': '눈에 좋은 음식'
}]


diseases = [{

'name': '갑상선기능저하증',

'explanation': '갑상선에서 분비되는 호르몬이 감소하는 질병을 말한다. 고령의 대형견에서 많이 발생한다. 기운이 없어지고 졸거나 추위를 타는 증상을 보인다. 피부가 거칠어지고 탈모가 증가한다. 단순한 노화 현상이라고 생각할 수 있어 발견이 늦어지는 경우가 있다. '

},{

'name': '이혈종',

'explanation': '귓바퀴의 피부와 연골 사이에 혈액이나 액체가 쌓여 부어오르는 질병이다. 귀를 심하게 긁거나 머리를 흔드는 등의 행동으로 혈관이 터져 이혈종이 발생하기도 한다. 귓바퀴 안쪽이 부어오르고 열과 통증이 동반된다. 환부가 크면 귀의 연골이 위축되면서 귓바퀴 모양이 변형되기도 한다. '

},{

'name': '추간판 헤르니아',

'explanation': '추간판은 등뼈의 사이사이에 위치해 뼈끼리 서로 부딪히지 않도록 하는 역할을 한다. 그런데 이 추간판이 튀어나오면 신경을 압박하고, 다양한 장애를 일으키게 된다. 심할 경우 하반신 마비가 될 수도 있다. 걷는 모습이나 자세가 이상하고, 등을 만지면 아파하는 모습을 보인다. 증상이 시해지면 하반신이 마비되어 앞다리로만 걷게 된다. '

},{

'name': '고관절형성부전',

'explanation': '고관절이 잘 발달하지 못해 대퇴골두가 빠지는 질병이다. '

},{

'name': '녹내장',

'explanation': '안압이 높아지고 빛을 쏘았을 때 눈이 녹색으로 보이는 질병이다.  '

}]

foods = [{

'name': '닭곰탕',

'image_src': 'http://blogfiles.naver.net/20110318_97/303mani_1300401830762tjBPv_JPEG/DSCF0713.JPG](http://blogfiles.naver.net/20110318_97/303mani_1300401830762tjBPv_JPEG/DSCF0713.JPG)',

'explanation': '관절에 좋은 음식'

},{

'name': '북엇국',

'image_src': 'https://blog.naver.com/wltnd87/221022202588](https://blog.naver.com/wltnd87/221022202588)',

'explanation': '관절에 좋은 음식'

},{

'name': '홍화씨가루',

'image_src': 'http://blogfiles.naver.net/MjAxNjEwMjhfMjM0/MDAxNDc3NjM0MjgxOTk2.xUd9secV3qEIxfX6m0okR4W_UEw83hyN9TetypXVswQg.RZ90woLTys_muYv7DixZivq3bXqwE1sLeU7g7c_5RnMg.JPEG.joytoseon/3.jpg](http://blogfiles.naver.net/MjAxNjEwMjhfMjM0/MDAxNDc3NjM0MjgxOTk2.xUd9secV3qEIxfX6m0okR4W_UEw83hyN9TetypXVswQg.RZ90woLTys_muYv7DixZivq3bXqwE1sLeU7g7c_5RnMg.JPEG.joytoseon/3.jpg)',

'explanation': '관절에 좋은 음식'

},{

'name': '단호박',

'image_src': 'http://blogfiles.naver.net/20160315_151/cwr9441_1458048688963eOWuc_JPEG/NaverBlog_20160315_223128_04.jpg](http://blogfiles.naver.net/20160315_151/cwr9441_1458048688963eOWuc_JPEG/NaverBlog_20160315_223128_04.jpg)',

'explanation': '눈, 관절에 좋은 음식'

},{

'name': '시금치',

'image_src': 'http://blogfiles.naver.net/20130205_265/smy2166_1360023315539Fwi1N_JPEG/16.JPG](http://blogfiles.naver.net/20130205_265/smy2166_1360023315539Fwi1N_JPEG/16.JPG)',

'explanation': '눈에 좋은 음식'

},{

'name': '당근',

'image_src': 'http://blogfiles.naver.net/20110831_204/qkrwlsdyd28_1314768481649KvnVp_JPEG/2011-08-31_14%3B26%3B24.jpg](http://blogfiles.naver.net/20110831_204/qkrwlsdyd28_1314768481649KvnVp_JPEG/2011-08-31_14%3B26%3B24.jpg)',

'explanation': '눈에 좋은 음식'

}]


beagle = dict()

beagle['species'] = 'beagle'
#age, image는 나중에 받아서 추가
# beagle['age'] = {0 : 0, 'adult' : 'adult', 'senior' : 'senior'}
beagle['origin'] = '영국'
beagle['avg_weight'] = {'male' : '10-11kg', 'female':'9-10kg'}

beagle['foods'] = foods
beagle['diseases'] = diseases
beagle['feature'] = '''비글은 하운드이며 그 중에서는 좀 작은 편이다. 사냥을 잘하는 개답게 머리가 좋고 행동이 민첩하다. 북미에서는 가장 인기있는 견종중의 하나이다. 비글은 지랄견의 왕자로 잘 알려져 있지만 그건 비글의 속성을 이해하지 못하고 개를 기르는 방법이 잘못되어서 그렇다. 개가 하는 대부분의 잘못은 기르는 사람에게서 기인하니 지랄견 이라 하기 전에 아래 사실을 유념해야 제대로 기를수가 있다. 비글은 자기 주장이 확고하고 꽤나 드센 성격을 가지고 있는 데다가 몸도 적당히 커서 억센 훈육이 필요하고 특히 운동량과 행동반경이 많이 필요하다. 비글은 개 중에서 친화력이 가장 좋은 견종으로 인정받고 있다. 비글은 헛짖음이 좀 있는 편이며, 식탐도 강한 편이다.'''


border_collie = dict()
border_collie['species'] = 'border_collie'
#age, image는 나중에 받아서 추가


border_collie['avg_weight'] = {'male' : '30kg', 'female':'25kg'}

border_collie['foods'] = border_foods
border_collie['diseases'] = border_diseases
border_collie['feature'] = '''세계에서 가장 머리가 좋은 개로 유명하며 그 명성에 걸맞게 학습 능력이 매우 뛰어나다. 
또한 어린 보더 콜리와 나이 든 보더 콜리를 같이 기르면, 노견이 어린 개에게 주인이 사용하는 명령어와 기타 생활에 대해 교육한다고 한다. 
지능이 어느 정도냐면 3살짜리 어린아이의 지능과 동등할 정도. '체이서'라는 한 개체는 무려 1,022 단어를 알아 가장 많은 단어를 아는 개로 기네스북에 등재되어 있다. 
이뿐만이 아니라 명사와 동사의 차이를 구별할 수 있어 조합된 명령을 구별해서 알아듣고, 모르는 장난감의 이름을 들었을 때 소거법으로 유추해 내는 능력까지 있다.'''


